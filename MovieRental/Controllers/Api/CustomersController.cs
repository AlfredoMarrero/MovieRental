﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using MovieRental.Dtos;
using MovieRental.Models;
using System.Data.Entity;

namespace MovieRental.Controllers.Api
{
    public class CustomersController : ApiController
    {
         private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }


        //GET api/customer
        [HttpGet]
        public IHttpActionResult GetCustomers()
        {
            var customerDto = _context.Customers
                .Include(c => c.MembershipType)
                .ToList()
                .Select(Mapper.Map<Customer, CustomerDto>);

            return Ok(customerDto);
        }

        //GET api/customer/1
        [HttpGet]
        public IHttpActionResult GetCustomers(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
            {
                return NotFound();
            }

            var customerDto = Mapper.Map<Customer, CustomerDto>(customer);

            return Ok(customerDto);
        }

        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto) 
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _context.Customers.Add(customer);
            _context.SaveChanges();

            customerDto.Id = customer.Id;
            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDto);
        }

        [HttpPut]
        public void UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var customerInDb = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customerInDb == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            Mapper.Map<CustomerDto, Customer>(customerDto, customerInDb);


            _context.SaveChanges();
        }

        [HttpDelete]
        public void DeleteCusomer(int id)
        {
            var customerInDb = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customerInDb == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            } 

            _context.Customers.Remove(customerInDb);
            _context.SaveChanges();
        }

    }
}
