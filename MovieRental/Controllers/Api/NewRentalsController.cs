﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MovieRental.Dtos;
using MovieRental.Models;

namespace MovieRental.Controllers.Api
{
    public class NewRentalsController : ApiController
    {

        private ApplicationDbContext _context;

        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }

        //POST /api/newRentals/
        [HttpPost]
        public IHttpActionResult CreateNewRentals(NewRentalDto newRentalDto)
        {

            if (newRentalDto.MovieIds.Count == 0)
            {
                return BadRequest("Invalid movies Id");
            }

            var customer = _context.Customers.SingleOrDefault(c => c.Id == newRentalDto.CustomerId);

            if (customer == null)
            {
                return BadRequest("Invalid Customer Number");
            }

            var movies = _context.Movies.Where(c => newRentalDto.MovieIds.Contains(c.Id)).ToList();

            if (movies.Count != newRentalDto.MovieIds.Count)
            {
                return BadRequest("One or more moves are invalid.");
            }

            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                {
                    return BadRequest("Movie is not available.");
                }

                movie.NumberAvailable--;
                var rental = new Rental
                {
                    Customer = customer,
                    Movie = movie,
                    DateRented = DateTime.Now
                };
                _context.Rentals.Add(rental); 
            }

            _context.SaveChanges();

            return Ok();
        } 


    }
}
