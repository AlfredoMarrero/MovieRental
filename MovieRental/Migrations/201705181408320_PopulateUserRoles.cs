namespace MovieRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateUserRoles : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'97dd0799-b9c0-4c68-9200-5613a20942c9', N'admin@movierental.com', 0, N'ANlv4O/kY4z8w4vJ+bCILSgGoiL08qfDhiodnb2Zqy6HU0ouCP9DRpbo2wMNbWahyw==', N'fe64efd3-1417-4132-99e5-b39da8e04232', NULL, 0, 0, NULL, 1, 0, N'admin@movierental.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'dcb2923d-cfab-4195-aff9-e4fd9c5a966f', N'guest@movierental.com', 0, N'AEERWV71I01O+eqHkIcPn0YrpJe9tSlIIyieGBRBSR+0UaVeSbzGLm81OkZMhfLdqg==', N'4ed19b59-dc31-40a4-8435-7b5b83ad3989', NULL, 0, 0, NULL, 1, 0, N'guest@movierental.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'b57e29aa-9f9a-4ef6-9537-0b348b807d36', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'97dd0799-b9c0-4c68-9200-5613a20942c9', N'b57e29aa-9f9a-4ef6-9537-0b348b807d36')

");
        }
        
        public override void Down()
        {
        }
    }
}
